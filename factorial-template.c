#include <limits.h>  /* limits for integers */
#include <float.h>   /* limits for floats */
#include <stdio.h>
#include <math.h>

long maxlong(void) {

	return LONG_MAX;
}

long factorial(long n) {
	long ub=1;
	long i;
	for(i=1;i<n+1;i++){
			ub*=i;
		}
	return ub;
}

double upper_bound(long n) {
	long ub=1;
	int i;
	if (n>=6){
		if ((double)pow((float)(n/2),n)>maxlong()){
			ub=-1;
		}
		else{
			ub = pow((float)(n/2),n);
		}
	}
	if (n<6){
		if (n<0) {
			ub=-2;
		}
		else{
			for(i=1;i<n+1;i++){
				ub*=i;
			}
		ub += 1;
		}
	}	 	 
return ub;
}	 	 

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
    
    return 0;
}


